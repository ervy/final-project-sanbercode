<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Makanan;
use App\Models\Jenis;
use File;

class MakananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $makanan = Makanan::get();
        return view('makanan.index', ['makanan'=>$makanan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jenis = Jenis::get();
        return view('makanan.create', ['jenis'=>$jenis]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'price'=>'required',
            'stock'=>'required',
            'description'=>'required',
            'image'=>'required|image|mimes:jpg,png,jpeg'
        ]);

        $fileName = time().'.'.$request->image->extension();

        $request->image->move(public_path('images'), $fileName);

        $makanan = new Makanan;

        $makanan->name = $request->name;
        $makanan->price = $request->price;
        $makanan->stock = $request->stock;
        $makanan->jenis_id = $request->jenis_id;
        $makanan->description = $request->description;
        $makanan->image = $fileName;

        $makanan->save();

        return redirect('/food');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $makanan = Makanan::find($id);
        return view('page.shop-detail', ['makanan'=>$makanan]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $makanan = Makanan::find($id);
        $jenis = Jenis::get();
        return view('makanan.edit', ['makanan'=>$makanan, 'jenis'=>$jenis]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'price'=>'required',
            'stock'=>'required',
            'description'=>'required',
            'image'=>'image|mimes:jpg,png,jpeg'
        ]);

        $makanan = Makanan::find($id);

        if ($request->has('image')) {
            $path = 'images/';
            File::delete($path.$makanan->image);

            $fileName = time().'.'.$request->image->extension();

            $request->image->move(public_path('images'), $fileName);

            $makanan->image = $fileName;

            $makanan->save();
        }

        $makanan->name = $request->name;
        $makanan->price = $request->price;
        $makanan->stock = $request->stock;
        $makanan->jenis_id = $request->jenis_id;
        $makanan->description = $request->description;

        $makanan->save();

        return redirect('/food');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $makanan = Makanan::find($id);

        $path = 'images/';
        File::delete($path.$makanan->image);

        $makanan->delete();

        return redirect('/food');
    }

}
