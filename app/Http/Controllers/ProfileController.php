<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Profile;
use App\Models\User;

class ProfileController extends Controller
{
    public function index(){
        $iduser =Auth::id();
        $detailProfile = Profile::where('user_id',$iduser)->first();

        return view ('profile.update',['detailProfile'=>$detailProfile]);
    }
    public function update(Request $request, $id)
    {
        $request-> validate([
            'name' => 'required',
            'age' => 'required',
            'address' => 'required',
        ]);


        $profile = Profile::find($id);
        $profile->user->name = $request->name;
        $profile->age = $request->age;
        $profile->address = $request->address;

        $profile->push();

        return redirect('/profile');

    }
}
