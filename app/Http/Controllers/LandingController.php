<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Makanan;


class LandingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tampil()
    {
        $makanan = Makanan::get();
        return view('landing', ['makanan'=>$makanan]);
    }
}
