<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\MakananController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\JenisController;
use App\Models\Makanan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Landing page
Route::get('/', function () {
    return view('landing');
});

// Halaman edit profile
Route::resource('user', UserController::class);

// Memberikan Akses
Auth::routes();

Route::middleware(['auth'])->group(function(){

    Route::get('/shop-detail', function () {
        $makanan = Makanan::get();
        return view('makanan.makanan', ['makanan'=>$makanan]);
    });
    
    Route::get('/checkout', function () {
        return view('page.checkout');
    });
    
    Route::get('/contact', function () {
        return view('page.contact');
    });
    
    Route::get('/shoping-cart', function () {
        return view('page.shoping-cart');
    });

   Route::resource('profile', ProfileController::class)->only(['index','update']); 

    // Route makanan
    Route::resource('food', MakananController::class);

    // Route jenis makanan
    Route::resource('jenis', JenisController::class);

});
