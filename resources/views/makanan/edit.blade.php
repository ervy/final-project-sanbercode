@extends('partial.layout')

@section('content')

   <!-- Header Section Begin -->
   @include('partial.nav');
   <!-- Hero Section End -->
   
   <!-- Breadcrumb Section Begin -->
   <section class="breadcrumb-section set-bg mb-4" data-setbg="{{asset('/template/img/breadcrumb.jpg')}}">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="breadcrumb__text">
                    <h2>Add Food For Sell</h2>
                    <div class="breadcrumb__option">
                        <a href="/">Home</a>
                        <span>Add your Food</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Section End -->

<div class="container">
    <div class="card">
        <div class="card-body">
            <form action="/food/{{$makanan->id}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('Put')

                <form>
                    {{-- food name --}}
                    <div class="mb-3">
                      <label for="inputFoodName" class="form-label">Food Name</label>
                      <input type="text" name="name" class="form-control" value="{{$makanan->name}}" id="inputFoodName">
                    </div>
                    @error('name')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror

                    {{-- food price --}}
                    <div class="mb-3">
                      <label for="inputPrice" class="form-label">Price</label>
                      <input type="number" name="price" class="form-control" value="{{$makanan->price}}" id="inputPrice">
                    </div>
                    @error('price')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror

                    {{-- food stock --}}
                    <div class="mb-3">
                        <label for="inputStock" class="form-label">Stock</label>
                        <input type="number" name="stock" class="form-control" value="{{$makanan->stock}}" id="inputStock">
                    </div>
                    @error('stock')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror

                    {{-- food category --}}
                    <div class="mb-3">
                        <label for="inputStock" class="form-label">Choose Category</label>
                        <br>
                        <select class="form-control" name="jenis_id">
                            @forelse ($jenis as $item)
                            @if ($item->id === $makanan->jenis_id)
                                <option value="{{$item->id}}" selected>{{$item->name}}</option>
                            @else
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endif
                            @empty
                                <option value="">Data tidak tersedia</option>
                            @endforelse
                        </select>
                        <br><br>
                    </div>
                    @error('jenis_id')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror

                    {{-- food description --}}
                    <div class="mb-3">
                        <label for="inputDescription" class="form-label">Description</label>
                        <br>
                        <textarea name="description" id="inputDescription" class="form-control" cols="30" rows="10">{{$makanan->description}}</textarea>
                    </div>
                    @error('description')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror

                    {{-- food image --}}
                    <div class="mb-3">
                        <label for="inputimage" class="form-label">Image</label>
                        <input type="file" name="image" class="form-control" id="inputimage" >
                    </div>
                    @error('image')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror

                    {{-- submit --}}
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a href="/food" class="btn btn-warning">Cancel</a>
            </form>
        </div>
    </div>
</div>

 <!-- Footer Section Begin -->
 @include('partial.footer')
 <!-- Footer Section End -->

 <!-- Js Plugins -->
 <script src="{{asset('/template/js/jquery-3.3.1.min.js')}}"></script>
 <script src="{{asset('/template/js/bootstrap.min.js')}}"></script>
 <script src="{{asset('/template/js/jquery.nice-select.min.js')}}"></script>
 <script src="{{asset('/template/js/jquery-ui.min.js')}}"></script>
 <script src="{{asset('/template/js/jquery.slicknav.js')}}"></script>
 <script src="{{asset('/template/js/mixitup.min.js')}}"></script>
 <script src="{{asset('/template/js/owl.carousel.min.js')}}"></script>
 <script src="{{asset('/template/js/main.js')}}"></script>

@endsection