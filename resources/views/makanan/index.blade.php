@extends('partial.layout')

@section('content')

   <!-- Header Section Begin -->
   @include('partial.nav');
   <!-- Hero Section End -->

    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="{{asset('/template/img/breadcrumb.jpg')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Add Food For Sell</h2>
                        <div class="breadcrumb__option">
                            <a href="/">Home</a>
                            <span>Add your Food</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Shoping Cart Section Begin -->
    <section class="shoping-cart spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shoping__cart__table">
                        <div class="shoping__cart__btns mb-3">
                            <a href="/food/create" class="primary-btn cart-btn">Add Food</a>
                        </div>
                        <table class="table table-striped">
                            
                                <thead>
                                  <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Stock</th>
                                    <th scope="col">Price</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    @forelse ($makanan as $key=>$value)
                                        <tr>
                                            <td>{{$key + 1}}</th>
                                            <td>{{$value->name}}</td>
                                            <td>{{$value->stock}}</td>
                                            <td>{{$value->price}}</td>
                                            <td>
                                                <form action="/food/{{$value->id}}" method="POST">
                                                @method('delete')
                                                @csrf
                                                <a href="/food/{{$value->id}}" class="btn btn-primary btn-sm">Detail</a>
                                                <a href="/food/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                                <input type="submit" value="delete" class="btn btn-danger btn-sm">
                                            </form>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr colspan="3">
                                            <td>No data</td>
                                        </tr>  
                                    @endforelse              
                                </tbody>
                          </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Shoping Cart Section End -->

    <!-- Footer Section Begin -->
    @include('partial.footer')
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="{{asset('/template/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('/template/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('/template/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('/template/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('/template/js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('/template/js/mixitup.min.js')}}"></script>
    <script src="{{asset('/template/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('/template/js/main.js')}}"></script>

@endsection