@extends('partial.layout')

@section('content')

   <!-- Header Section Begin -->
   @include('partial.nav');
   <!-- Hero Section End -->
   
   <!-- Breadcrumb Section Begin -->
   <section class="breadcrumb-section set-bg mb-4" data-setbg="{{asset('/template/img/breadcrumb.jpg')}}">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="breadcrumb__text">
                    <h2>Add Food For Sell</h2>
                    <div class="breadcrumb__option">
                        <a href="/">Home</a>
                        <span>Add your Food</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Section End -->

<div class="container">
            <div class="row row-cols-1 row-cols-md-3 g-4">
                @forelse ($makanan as $item)
                <div class="col mb-4">
                  <div class="card h-100 ">
                    <img src="{{asset('images/'.$item->image)}}" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h3 class="card-title">{{$item->name}}</h3>
                      <h5 class="card-price">Harga : Rp.{{$item->price}},-</h5>
                      <p class="card-text">{{Str::limit($item->description, 30)}}</p>
                      <a href="/food/{{$item->id}}" class="btn btn-primary btn-block">Read Me</a>
                    </div>
                  </div>
                </div>
        @empty
            Data Tidak Tersedia
        @endforelse
    </div>
</div>


 <!-- Footer Section Begin -->
 @include('partial.footer')
 <!-- Footer Section End -->

 <!-- Js Plugins -->
 <script src="{{asset('/template/js/jquery-3.3.1.min.js')}}"></script>
 <script src="{{asset('/template/js/bootstrap.min.js')}}"></script>
 <script src="{{asset('/template/js/jquery.nice-select.min.js')}}"></script>
 <script src="{{asset('/template/js/jquery-ui.min.js')}}"></script>
 <script src="{{asset('/template/js/jquery.slicknav.js')}}"></script>
 <script src="{{asset('/template/js/mixitup.min.js')}}"></script>
 <script src="{{asset('/template/js/owl.carousel.min.js')}}"></script>
 <script src="{{asset('/template/js/main.js')}}"></script>

@endsection