@extends('partial.layout')

@section('content')

   <!-- Header Section Begin -->
   @include('partial.nav');
   <!-- Hero Section End -->
   
   <!-- Breadcrumb Section Begin -->
   <section class="breadcrumb-section set-bg mb-4" data-setbg="{{asset('/template/img/breadcrumb.jpg')}}">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="breadcrumb__text">
                    <h2>Add Food For Sell</h2>
                    <div class="breadcrumb__option">
                        <a href="/">Home</a>
                        <span>Add your Food</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Section End -->

   <div class="container">
    <div class="card">
        <div class="card-body">
            <form action="/food" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                  <label for="inputFoodName">Food Name</label>
                  <input type="text" name="name" class="form-control" id="inputFoodName">
                </div>
                @error('name')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
                <div class="form-group">
                    <label for="inputPrice">Price</label>
                    <input type="number" name="price" class="form-control" id="inputPrice">
                </div>
                @error('price')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
                <div class="form-group">
                    <label for="inputStock">Stock</label>
                    <input type="number" name="stock" class="form-control" id="inputStock">
                </div>
                @error('stock')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
                <div class="form-group">
                    <label for="inputStock">Jenis</label>
                    <select class="form-control" name="jenis_id">
                        <option value="">--pilih jenis--</option>
                        @forelse ($jenis as $item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                        @empty
                        <option value="">Data tidak tersedia</option>
                        @endforelse
                    </select>
                </div>
                @error('jenis_id')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
                <div class="form-group">
                    <label for="inputDescription">Description</label>
                    <textarea name="description" id="inputDescription" class="form-control" cols="30" rows="10"></textarea>
                </div>
                @error('description')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
                <div class="form-group">
                    <label for="inputimage">Gambar</label>
                    <input type="file" name="image" class="form-control" id="inputimage">
                </div>
                @error('image')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="/food" class="btn btn-warning btn-right">Cancel</a>
            </form>
        </div>
        
    </div>
</div>

 <!-- Footer Section Begin -->
 @include('partial.footer')
 <!-- Footer Section End -->

 <!-- Js Plugins -->
 <script src="{{asset('/template/js/jquery-3.3.1.min.js')}}"></script>
 <script src="{{asset('/template/js/bootstrap.min.js')}}"></script>
 <script src="{{asset('/template/js/jquery.nice-select.min.js')}}"></script>
 <script src="{{asset('/template/js/jquery-ui.min.js')}}"></script>
 <script src="{{asset('/template/js/jquery.slicknav.js')}}"></script>
 <script src="{{asset('/template/js/mixitup.min.js')}}"></script>
 <script src="{{asset('/template/js/owl.carousel.min.js')}}"></script>
 <script src="{{asset('/template/js/main.js')}}"></script>

@endsection