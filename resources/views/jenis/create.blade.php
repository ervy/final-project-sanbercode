@extends('partial.layout')

@section('content')

   <!-- Header Section Begin -->
   @include('partial.nav');
   <!-- Hero Section End -->
   
   <!-- Breadcrumb Section Begin -->
   <section class="breadcrumb-section set-bg" data-setbg="{{asset('/template/img/breadcrumb.jpg')}}">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="breadcrumb__text">
                    <h2>Add Food For Sell</h2>
                    <div class="breadcrumb__option">
                        <a href="/">Home</a>
                        <span>Add your Category Food</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Section End -->

   <div class="container">
    <div class="card m-4">
        <div class="card-body">
            <form action="/jenis" method="POST">
                @csrf
                <div class="form-group">
                  <label>Category Name</label>
                  <input type="text" name="name" class="form-control">
                </div>
                @error('name')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="/jenis" class="btn btn-warning btn-right">Cancel</a>
            </form>
        </div>
        
    </div>
</div>

 <!-- Footer Section Begin -->
 @include('partial.footer')
 <!-- Footer Section End -->

 <!-- Js Plugins -->
 <script src="{{asset('/template/js/jquery-3.3.1.min.js')}}"></script>
 <script src="{{asset('/template/js/bootstrap.min.js')}}"></script>
 <script src="{{asset('/template/js/jquery.nice-select.min.js')}}"></script>
 <script src="{{asset('/template/js/jquery-ui.min.js')}}"></script>
 <script src="{{asset('/template/js/jquery.slicknav.js')}}"></script>
 <script src="{{asset('/template/js/mixitup.min.js')}}"></script>
 <script src="{{asset('/template/js/owl.carousel.min.js')}}"></script>
 <script src="{{asset('/template/js/main.js')}}"></script>

@endsection