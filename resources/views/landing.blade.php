<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Ogani Template">
    <meta name="keywords" content="Ogani, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kelompok 9</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{ asset('/template/css/bootstrap.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/template/css/font-awesome.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/template/css/elegant-icons.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/template/css/nice-select.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/template/css/jquery-ui.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/template/css/owl.carousel.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/template/css/slicknav.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/template/css/style.css') }}" type="text/css">
</head>

<body>

    <!-- Header Section Begin -->
    <header class="header">
        <div class="header__top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="header__top__left">
                            <ul>
                                <li><i class="fa fa-envelope"></i> hello@colorlib.com</li>
                                <li>Free Shipping for all Order of $99</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="header__top__right">
                            <div class="header__top__right__social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                            </div>
                            @guest
                                <div class="header__top__right__auth">
                                    <a href="/login"><i class="fa fa-user"></i> Login</a>
                                </div>
                            @endguest
    
                            @auth
                                <div class="header__top__right__auth">
                                    <a href="/profile" style="margin-right: 30px"><i class="fa fa-user"></i>{{ Auth::user()->name }}   </a>
                                </div>
                                <div class="header__top__right__auth" aria-labelledby="navbarDropdown">
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
    
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            @endauth
    
    
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="header__logo">
                        <a href="/"><img src="{{ asset('/template/img/logo.png') }}" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <nav class="header__menu">
                        <ul>
                            <li class="active"><a href="/">Home</a></li>
                            <li><a href="#">Pages</a>
                                <ul class="header__menu__dropdown">
                                    <li><a href="/shoping-cart">Shoping Cart</a></li>
                                    <li><a href="/checkout">Check Out</a></li>
                                </ul>
                            </li>
                            <li><a href="#">As Seller.</a>
                                <ul class="header__menu__dropdown">
                                    <li><a href="/jenis">New Category Food</a></li>
                                    <li><a href="/food">Your Food Table</a></li>
                                    <li><a href="/shop-detail">Product Details</a></li>
                                </ul>
                            </li>
                            <li><a href="/contact">Contact</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-3">
                    <div class="header__cart">
                        <ul>
                            <li><a href="#" id="like" onClick="clearLike()"><i class="fa fa-heart"></i><span>0</span></a></li>
                            <li>
                                <a href="#" id="cart" onClick="clearCart()"><i class="fa fa-shopping-bag"></i><span>0</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="humberger__open">
                <i class="fa fa-bars"></i>
            </div>
        </div>
    </header>
    <!-- Header Section End -->
    
    <!-- Hero Section Begin -->
    <section class="hero">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="hero__categories">
                        <div class="hero__categories__all">
                            <i class="fa fa-bars"></i>
                            <span>All departments</span>
                        </div>
                        <ul>
                            <li><a href="#">Fresh Meat</a></li>
                            <li><a href="#">Vegetables</a></li>
                            <li><a href="#">Fruit & Nut Gifts</a></li>
                            <li><a href="#">Fresh Berries</a></li>
                            <li><a href="#">Ocean Foods</a></li>
                            <li><a href="#">Butter & Eggs</a></li>
                            <li><a href="#">Fastfood</a></li>
                            <li><a href="#">Fresh Onion</a></li>
                            <li><a href="#">Papayaya & Crisps</a></li>
                            <li><a href="#">Oatmeal</a></li>
                            <li><a href="#">Fresh Bananas</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="hero__search">
                        <div class="hero__search__form">
                            <form action="#">
                                <div class="hero__search__categories">
                                    All Categories
                                    <span class="arrow_carrot-down"></span>
                                </div>
                                <input type="text" placeholder="What do yo u need?">
                                <button type="submit" class="site-btn">SEARCH</button>
                            </form>
                        </div>
                        <div class="hero__search__phone">
                            <div class="hero__search__phone__icon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="hero__search__phone__text">
                                <h5>+65 11.188.888</h5>
                                <span>support 24/7 time</span>
                            </div>
                        </div>
                    </div>
                    <div class="hero__item set-bg" data-setbg="{{ asset('/template/img/hero/banner.jpg') }}">
                        <div class="hero__text">
                            <span>FRUIT FRESH</span>
                            <h2>Vegetable <br />100% Organic</h2>
                            <p>Free Pickup and Delivery Available</p>
                            <a href="#" class="primary-btn">SHOP NOW</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Hero Section End -->

    <!-- Categories Section Begin -->
    <section class="categories">
        <div class="container">
            <div class="row">
                <div class="categories__slider owl-carousel">
                    <div class="col-lg-3">
                        <div class="categories__item set-bg"
                            data-setbg="{{ asset('/template/img/categories/cat-1.jpg') }}">
                            <h5><a href="/shop-detail">Fresh Fruit</a></h5>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="categories__item set-bg"
                            data-setbg="{{ asset('/template/img/categories/cat-2.jpg') }}">
                            <h5><a href="/shop-detail">Dried Fruit</a></h5>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="categories__item set-bg"
                            data-setbg="{{ asset('/template/img/categories/cat-3.jpg') }}">
                            <h5><a href="/shop-detail">Vegetables</a></h5>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="categories__item set-bg"
                            data-setbg="{{ asset('/template/img/categories/cat-4.jpg') }}">
                            <h5><a href="/shop-detail">Drink Fruits</a></h5>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="categories__item set-bg"
                            data-setbg="{{ asset('/template/img/categories/cat-5.jpg') }}">
                            <h5><a href="/shop-detail">Fresh Meat</a></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Categories Section End -->

    <!-- Featured Section Begin -->
    <section class="featured spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Featured Product</h2>
                    </div>
                    <div class="featured__controls">
                        <ul>
                            <li class="active" data-filter="*">All</li>
                            <li data-filter=".oranges">Fruit</li>
                            <li data-filter=".fresh-meat">Fresh Meat</li>
                            <li data-filter=".vegetables">Vegetables</li>
                            <li data-filter=".fastfood">Fastfood</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row featured__filter">
                <div class="col-lg-3 col-md-4 col-sm-6 mix fresh-meat">
                    <div class="featured__item">
                        <div class="featured__item__pic set-bg"
                            data-setbg="{{ asset('/template/img/featured/feature-1.jpg') }}">
                            <ul class="featured__item__pic__hover">
                                <li><a href="#" id="addLike" onClick="addLike()"><i class="fa fa-heart"></i></a></li>
                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                <li>
                                    <a href="#" id="addCart" onClick="addCart()"><i class="fa fa-shopping-bag"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="featured__item__text">
                            <h6><a href="#">Meat</a></h6>
                            <h5>$30.00</h5>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 mix oranges">
                    <div class="featured__item">
                        <div class="featured__item__pic set-bg"
                            data-setbg="{{ asset('/template/img/featured/feature-2.jpg') }}">
                            <ul class="featured__item__pic__hover">
                                <li><a href="#" id="addLike" onClick="addLike()"><i class="fa fa-heart"></i></a></li>
                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                <li>
                                    <a href="#" id="addCart" onClick="addCart()"><i class="fa fa-shopping-bag"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="featured__item__text">
                            <h6><a href="#">Banana</a></h6>
                            <h5>$30.00</h5>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 mix oranges">
                    <div class="featured__item">
                        <div class="featured__item__pic set-bg"
                            data-setbg="{{ asset('/template/img/featured/feature-3.jpg') }}">
                            <ul class="featured__item__pic__hover">
                                <li><a href="#" id="addLike" onClick="addLike()"><i class="fa fa-heart"></i></a></li>
                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                <li>
                                    <a href="#" id="addCart" onClick="addCart()"><i class="fa fa-shopping-bag"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="featured__item__text">
                            <h6><a href="#">Guava</a></h6>
                            <h5>$30.00</h5>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 mix oranges">
                    <div class="featured__item">
                        <div class="featured__item__pic set-bg"
                            data-setbg="{{ asset('/template/img/featured/feature-4.jpg') }}">
                            <ul class="featured__item__pic__hover">
                                <li><a href="#" id="addLike" onClick="addLike()"><i class="fa fa-heart"></i></a></li>
                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                <li>
                                    <a href="#" id="addCart" onClick="addCart()"><i class="fa fa-shopping-bag"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="featured__item__text">
                            <h6><a href="#">Watermelon</a></h6>
                            <h5>$30.00</h5>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 mix oranges">
                    <div class="featured__item">
                        <div class="featured__item__pic set-bg"
                            data-setbg="{{ asset('/template/img/featured/feature-5.jpg') }}">
                            <ul class="featured__item__pic__hover">
                                <li><a href="#" id="addLike" onClick="addLike()"><i class="fa fa-heart"></i></a></li>
                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                <li>
                                    <a href="#" id="addCart" onClick="addCart()"><i class="fa fa-shopping-bag"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="featured__item__text">
                            <h6><a href="#">Grape</a></h6>
                            <h5>$30.00</h5>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 mix fastfood">
                    <div class="featured__item">
                        <div class="featured__item__pic set-bg"
                            data-setbg="{{ asset('/template/img/featured/feature-6.jpg') }}">
                            <ul class="featured__item__pic__hover">
                                <li><a href="#" id="addLike" onClick="addLike()"><i class="fa fa-heart"></i></a></li>
                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                <li>
                                    <a href="#" id="addCart" onClick="addCart()"><i class="fa fa-shopping-bag"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="featured__item__text">
                            <h6><a href="#">Hamburger</a></h6>
                            <h5>$30.00</h5>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 mix oranges">
                    <div class="featured__item">
                        <div class="featured__item__pic set-bg"
                            data-setbg="{{ asset('/template/img/featured/feature-7.jpg') }}">
                            <ul class="featured__item__pic__hover">
                                <li><a href="#" id="addLike" onClick="addLike()"><i class="fa fa-heart"></i></a></li>
                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                <li>
                                    <a href="#" id="addCart" onClick="addCart()"><i class="fa fa-shopping-bag"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="featured__item__text">
                            <h6><a href="#">Manggo</a></h6>
                            <h5>$30.00</h5>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 mix oranges">
                    <div class="featured__item">
                        <div class="featured__item__pic set-bg"
                            data-setbg="{{ asset('/template/img/featured/feature-8.jpg') }}">
                            <ul class="featured__item__pic__hover">
                                <li><a href="#" id="addLike" onClick="addLike()"><i class="fa fa-heart"></i></a></li>
                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                <li>
                                    <a href="#" id="addCart" onClick="addCart()"><i class="fa fa-shopping-bag"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="featured__item__text">
                            <h6><a href="#">Apple</a></h6>
                            <h5>$30.00</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Featured Section End -->

    <!-- Latest Product Section Begin -->
    <section class="latest-product spad">
        <div class="container">
            <div class="section-title">
                <h2>Featured Product</h2>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="latest-product__text">
                        <h4>Latest Products</h4>
                        <div class="latest-product__slider owl-carousel">
                            <div class="latest-prdouct__slider__item">
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{ asset('/template/img/latest-product/lp-1.jpg') }}"
                                            alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Spinach</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{ asset('/template/img/latest-product/lp-2.jpg') }}"
                                            alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Paprika</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{ asset('/template/img/latest-product/lp-3.jpg') }}"
                                            alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Fried Chicken</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                            </div>
                            <div class="latest-prdouct__slider__item">
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{ asset('/template/img/latest-product/lp-1.jpg') }}"
                                            alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Spinach</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{ asset('/template/img/latest-product/lp-2.jpg') }}"
                                            alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Paprika</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{ asset('/template/img/latest-product/lp-3.jpg') }}"
                                            alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Fried Chicken</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="latest-product__text">
                        <h4>Top Rated Products</h4>
                        <div class="latest-product__slider owl-carousel">
                            <div class="latest-prdouct__slider__item">
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{ asset('/template/img/latest-product/lp-1.jpg') }}"
                                            alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Spinach</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{ asset('/template/img/latest-product/lp-2.jpg') }}"
                                            alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Paprika</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{ asset('/template/img/latest-product/lp-3.jpg') }}"
                                            alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Fried Chicken</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                            </div>
                            <div class="latest-prdouct__slider__item">
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{ asset('/template/img/latest-product/lp-1.jpg') }}"
                                            alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{ asset('/template/img/latest-product/lp-2.jpg') }}"
                                            alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{ asset('/template/img/latest-product/lp-3.jpg') }}"
                                            alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="latest-product__text">
                        <h4>Review Products</h4>
                        <div class="latest-product__slider owl-carousel">
                            <div class="latest-prdouct__slider__item">
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{ asset('/template/img/latest-product/lp-1.jpg') }}"
                                            alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{ asset('/template/img/latest-product/lp-2.jpg') }}"
                                            alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{ asset('/template/img/latest-product/lp-3.jpg') }}"
                                            alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                            </div>
                            <div class="latest-prdouct__slider__item">
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{ asset('/template/img/latest-product/lp-1.jpg') }}"
                                            alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{ asset('/template/img/latest-product/lp-2.jpg') }}"
                                            alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="{{ asset('/template/img/latest-product/lp-3.jpg') }}"
                                            alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>Crab Pool Security</h6>
                                        <span>$30.00</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Latest Product Section End -->

    <!-- Footer Section Begin -->
    @include('partial.footer');
    <!-- Footer Section End -->

    <script>
    
    // Tambah makanan ke keranjang
        var keranjang = document.getElementById('cart')
        var keranjangNumber = keranjang.value

        function addCart() 
        {
            keranjangNumber++
            if (keranjangNumber >= 10) {
                keranjang.innerHTML = `<i class="fa fa-shopping-bag"></i><span>9+</span>`
            } else {
                keranjang.innerHTML = `<i class="fa fa-shopping-bag"></i><span>${keranjangNumber}</span>`
            }
        }

        function clearCart() 
        {
            keranjangNumber = 0
            keranjang.innerHTML = `<i class="fa fa-shopping-bag"></i><span>${keranjangNumber}</span>`
        }
    

    // Like makanan
        var suka = document.getElementById('like')
        var sukaNumber = suka.value

        function addLike() 
        {
            sukaNumber++
            if (sukaNumber >= 10) {
                suka.innerHTML = `<i class="fa fa-heart"></i><span>9+</span>`
            } else {
                suka.innerHTML = `<i class="fa fa-heart"></i><span>${sukaNumber}</span>`
            }
        }

        function clearLike() 
        {
            sukaNumber = 0
            suka.innerHTML = `<i class="fa fa-heart"></i><span>${sukaNumber}</span>`
        }
    </script>

    <!-- Js Plugins -->
    <script src="{{ asset('/template/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('/template/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/template/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('/template/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('/template/js/jquery.slicknav.js') }}"></script>
    <script src="{{ asset('/template/js/mixitup.min.js') }}"></script>
    <script src="{{ asset('/template/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('/template/js/main.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>



</body>

</html>
