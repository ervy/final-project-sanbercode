@extends('profile.layout')

@section('content')

<div class="container">
    <h1>Profile</h1>
    <div class="card">
        <div class="card-body">
            <form action="/profile/{{$detailProfile->id}}" method="POST">
                @csrf
                @method('put')
                <div class="form-group">
                  <label >Name</label>
                  <input type="text" name="name" class="form-control" value="{{$detailProfile->user->name}}">
                </div>
                @error('name')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
                <div class="form-group">
                    <label >Age</label>
                    <input type="number" name="age" class="form-control" value="{{$detailProfile->age}}">
                </div>
                @error('age')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
             
                <div class="form-group">
                    <label >Address</label>
                    <textarea name="address"  class="form-control">{{$detailProfile->address}}"</textarea>
                </div>
                @error('address')
                    <div class="alert alert-danger">{{$message}}</div>
                @enderror
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Tambah</button>
                    <a href="/profile" class="btn btn-warning">Cancel</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection